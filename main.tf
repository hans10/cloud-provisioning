
locals {
    aws_region = "eu-west-1"
}
provider "aws" {
    region = local.aws_region
}

resource "aws_vpc" "my_vpc" {
    cidr_block = "10.0.0.0/16"
    enable_dns_hostnames = true
}

resource "aws_internet_gateway" "igw" {
    vpc_id = aws_vpc.my_vpc.id
    tags = {
        Name = "main gw"
    }
}

module "subnets" {
    source = "git::https://github.com/cloudposse/terraform-aws-dynamic-subnets.git?ref=0.18.1"
    name = "demo"
    vpc_id = aws_vpc.my_vpc.id
    igw_id = aws_internet_gateway.igw.id
    cidr_block = aws_vpc.my_vpc.cidr_block
    availability_zones = [
        "${local.aws_region}a",
        "${local.aws_region}b"]
}

module "beanstalk_iam" {
    source = "./beanstalk_iam"
}


module "demo_app" {
    source = "./demo_app"
    vpc_id = aws_vpc.my_vpc.id

    pubnet1 = module.subnets.public_subnet_ids[0]
    pubnet2 = module.subnets.public_subnet_ids[1]

    privnet1 = module.subnets.private_subnet_ids[0]
    privnet2 = module.subnets.private_subnet_ids[1]

    private_cidrs = module.subnets.private_subnet_cidrs
    service_role_arn = module.beanstalk_iam.service_role_arn
    instance_profile_arn = module.beanstalk_iam.instance_profile_arn
}
