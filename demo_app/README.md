 **Gotchas:**

When changing any of the beanstalk settings, you need to create a new version of the template
and apply it to the environment. Sadly, the terraform-beanstalk-env plugin can not do this automatically.

This is a known bug.
