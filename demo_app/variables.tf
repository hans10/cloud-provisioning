variable "vpc_id" {
    type = string
    description = "the VPC in which we live"
}

variable "pubnet1" {
    type = string
}

variable "pubnet2" {
    type = string
}

variable "privnet1" {
    type = string
}

variable "privnet2" {
    type = string
}

variable "private_cidrs" {
    type = list(string)
}

variable "service_role_arn" {
    type = string
    description = "ARN of the Beanstalk service role that will be attached to the app"
}

variable "instance_profile_arn" {
    type = string
}
