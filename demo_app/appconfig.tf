resource "aws_ssm_parameter" "some_config" {
    name  = "/config/demo/myvar"
    type  = "String"
    value = "my val!"
}
