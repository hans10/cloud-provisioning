resource "aws_elastic_beanstalk_application" "demo_app" {
    name = "demo"
    description = "Demo app"

    appversion_lifecycle {
        service_role = var.service_role_arn
        max_count = 128
        delete_source_from_s3 = true
    }
}

resource "aws_elastic_beanstalk_configuration_template" "demo_config_template_v1" {
    // be aware: when changing any of this, you need to create a new version of the template
    // and apply it to the actual aws_elastic_beanstalk_application_env.

    application = aws_elastic_beanstalk_application.demo_app.name
    name = "demo-app-config-template"
    solution_stack_name = "64bit Amazon Linux 2018.03 v2.9.1 running PHP 7.3"

    setting {
        namespace = "aws:elasticbeanstalk:command"
        name = "IgnoreHealthCheck"
        value = true
    }

    setting {
        namespace = "aws:ec2:vpc"
        name = "VPCId"
        value = var.vpc_id
    }

    setting {
        namespace = "aws:ec2:vpc"
        name      = "Subnets"
        value     = "${var.privnet1},${var.privnet2}"
    }

    setting {
        namespace = "aws:ec2:vpc"
        name      = "ELBSubnets"
        value     = "${var.pubnet1},${var.pubnet2}"
    }

    setting {
        namespace = "aws:ec2:vpc"
        name      = "AssociatePublicIpAddress"
        value     = "false"
    }

    setting {
        namespace = "aws:elasticbeanstalk:environment:process:default"
        name = "MatcherHTTPCode"
        value = "200"
    }
    setting {
        namespace = "aws:elasticbeanstalk:environment:process:default"
        name = "HealthCheckTimeout"
        value = "3"
    }

    setting {
        namespace = "aws:elasticbeanstalk:managedactions"
        name = "ManagedActionsEnabled"
        value = true
    }
    setting {
        namespace = "aws:elasticbeanstalk:managedactions"
        name = "PreferredStartTime"
        value = "TUE:10:30"
    }

    setting {
        namespace = "aws:elasticbeanstalk:managedactions:platformupdate"
        name = "UpdateLevel"
        value = "minor"
    }
    setting {
        namespace = "aws:elasticbeanstalk:managedactions:platformupdate"
        name = "InstanceRefreshEnabled"
        value = true
    }

    setting {
        namespace = "aws:elbv2:listener:default"
        name = "ListenerEnabled"
        value = true
    }

    setting {
        namespace = "aws:elbv2:listener:80"
        name = "ListenerEnabled"
        value = true
    }

    setting {
        namespace = "aws:elbv2:listener:443"
        name = "ListenerEnabled"
        value = false
    }
    setting {
        namespace = "aws:elbv2:listener:80"
        name = "DefaultProcess"
        value = "default"
    }
    setting {
        namespace = "aws:elbv2:listener:80"
        name = "Protocol"
        value = "HTTP"
    }
    setting {
        namespace = "aws:elbv2:listener:80"
        name = "Rules"
        value = ""
    }
    setting {
        namespace = "aws:autoscaling:asg"
        name = "MaxSize"
        value = "2"
    }

    setting {
        namespace = "aws:autoscaling:asg"
        name = "MinSize"
        value = "1"
    }

    setting {
        namespace = "aws:elasticbeanstalk:cloudwatch:logs"
        name = "StreamLogs"
        value = true
    }

    setting {
        namespace = "aws:elasticbeanstalk:cloudwatch:logs"
        name = "RetentionInDays"
        value = 7
    }

    setting {
        namespace = "aws:elasticbeanstalk:environment"
        name = "ServiceRole"
        value = var.service_role_arn
    }

    setting {
        namespace = "aws:elasticbeanstalk:environment"
        name = "LoadBalancerType"
        value = "application"
    }

    setting {
        namespace = "aws:elasticbeanstalk:healthreporting:system"
        name = "SystemType"
        value = "enhanced"
    }

    setting {
        namespace = "aws:autoscaling:launchconfiguration"
        name = "InstanceType"
        value = "t1.micro"
    }
    setting {
        namespace = "aws:autoscaling:launchconfiguration"
        name = "IamInstanceProfile"
        value = var.instance_profile_arn
    }
}

resource "aws_elastic_beanstalk_environment" "my_env" {
    name = "demo-env"
    application = aws_elastic_beanstalk_application.demo_app.name
    template_name = aws_elastic_beanstalk_configuration_template.demo_config_template_v1.name
    tier = "WebServer"
    wait_for_ready_timeout = "10m"
}

