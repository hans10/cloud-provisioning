# Cloud-infra provisioning with Terraform

This repo contains [Terraform](https://www.terraform.io/intro/index.html) templates to provision our 
cloud/aws infrastructure elements.

Terraform is a tool that allows us to treat infrastructure as code. So, it makes sense to have a 
deployment pipeline for it just like we do for regular applications.

Here's what the pipeline looks like:

```
+--------------+      +----------+      +------------+
|              |      |          |      |            |
|              |      |          |      |            |
|   validate   | +--> |   plan   | +--> |   apply    |
|              |      |          |      |            |
|              |      |          |      |            |
+--------------+      +----------+      +------------+
```

The `terraform validate` and `terraform plan` stages are always executed automatically. They have no effect on existing infra.

The `apply` step is where things get interesting.

The `master` branch does not allow direct committing and PR's must be reviewed prior to merging.

For feature-branch, the `apply` stage must be executed manually.

# Your assignment

 - Setup an AWS CLI profile with `aws configure`
 - Once: `terraform init`
 - See that you're set: `terraform validate`
 - Make a plan (and include the var-file for your env) `terraform plan -out my.plan`
 - Execute the plan: `terraform apply my.plan`

 For funsies: run `terraform destroy` and then browse your resources in the console. After that, re-create everything.

# Questions for you to answer

1. Elastic Beanstalk is pretty old and allows you to create multiple 'environments' for an application.
These days, AWS-architects prefer to use separate AWS accounts for Test/Acc/Prod. 
Why do you think this is? Do use Google to answer!

2. The execution steps above can be automated. Doing that with Cloud resources takes CD-principles to a whole new level. 
How is using Terraform different than using AWS Cloudformation? Again, Google is your friend.
