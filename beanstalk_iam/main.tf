resource "aws_iam_role" "beanstalk_service_role" {
    name = "aws-beanstalk-service-role"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "elasticbeanstalk.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "beanstalk_ec2_instance_profile" {
    name  = "aws-beanstalk-ec2-instance-profile"
    role = aws_iam_role.beanstalk_ec2_role.name
}

resource "aws_iam_role" "beanstalk_ec2_role" {
    name = "aws-beanstalk-ec2-role"

    assume_role_policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

data "aws_iam_policy" "beanstalk_policy" {
    arn = "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkService"
}

data "aws_iam_policy" "beanstalk_enhanced_health_policy" {
    arn = "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkEnhancedHealth"
}

data "aws_iam_policy" "ec2_instance_policy_webtier" {
    arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier"
}

data "aws_iam_policy" "ec2_instance_policy_workertier" {
    arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier"
}
data "aws_iam_policy" "ec2_instance_policy_s3_readonly" {
    arn = "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"
}

data "aws_iam_policy" "ec2_instance_policy_ecr_readonly" {
    arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

data "aws_iam_policy_document" "read_parameters_from_aws_parameter_store" {
    statement {
        effect = "Allow"
        actions = ["ssm:GetParametersByPath"]
        resources = ["*"]
    }
}


resource "aws_iam_policy" "instance_profile_policy_read_parameters_from_aws_parameter_store" {
    name = "read-parameters-from-aws-parameter-store"
    policy = data.aws_iam_policy_document.read_parameters_from_aws_parameter_store.json
}

resource "aws_iam_role_policy_attachment" "instance_profile_attach_s3_readonly" {
    role       = aws_iam_role.beanstalk_ec2_role.name
    policy_arn = data.aws_iam_policy.ec2_instance_policy_s3_readonly.arn
}

resource "aws_iam_role_policy_attachment" "instance_profile_attach_ecr_readonly" {
    role       = aws_iam_role.beanstalk_ec2_role.name
    policy_arn = data.aws_iam_policy.ec2_instance_policy_ecr_readonly.arn
}

resource "aws_iam_role_policy_attachment" "instance_profile_attach_read_parameters_from_aws_parameter_store" {
    role       = aws_iam_role.beanstalk_ec2_role.name
    policy_arn = aws_iam_policy.instance_profile_policy_read_parameters_from_aws_parameter_store.arn
}

resource "aws_iam_role_policy_attachment" "webtier" {
    role =  aws_iam_role.beanstalk_ec2_role.name
    policy_arn = data.aws_iam_policy.ec2_instance_policy_webtier.arn
}

resource "aws_iam_role_policy_attachment" "workertier" {
    role =  aws_iam_role.beanstalk_ec2_role.name
    policy_arn = data.aws_iam_policy.ec2_instance_policy_workertier.arn
}

resource "aws_iam_role_policy_attachment" "beanstalk_attach" {
    role       = aws_iam_role.beanstalk_service_role.name
    policy_arn = data.aws_iam_policy.beanstalk_policy.arn
}

resource "aws_iam_role_policy_attachment" "beanstalk_enhanced_health_attach" {
    role       = aws_iam_role.beanstalk_service_role.name
    policy_arn = data.aws_iam_policy.beanstalk_enhanced_health_policy.arn
}
