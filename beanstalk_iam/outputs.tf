output "service_role_arn" {
    description = "ARN of the beanstalk service role"
    value = aws_iam_role.beanstalk_service_role.arn
}

output "instance_profile_arn" {
    description = "ec2 instance profile"
    value = aws_iam_instance_profile.beanstalk_ec2_instance_profile.arn
}
