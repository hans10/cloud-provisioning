resource "aws_iam_user" "ci" {
    name = "ci"
}

locals {
    ci_user_policies = [
        "arn:aws:iam::aws:policy/AWSElasticBeanstalkFullAccess",
        "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryPowerUser"
    ]
}
resource "aws_iam_user_policy_attachment" "ci_user_policy_attachments" {
    for_each  = toset(local.ci_user_policies)
    policy_arn = each.value
    user = aws_iam_user.ci.name
}
