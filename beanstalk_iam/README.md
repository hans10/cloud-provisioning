All the required roles and policies so our Beanstalk apps can:

 * Stream logs to cloudwatch
 * Read from ECR
 * Have enhanced health reporting
